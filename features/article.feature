Feature: Manage My Posts
  In order to make a blog
  As an author
  I want to create and manage the posts

  Background:
    Given I am on the log in page
    And I fill my credential
    And I click Log in
    Then I should see "Signed in successfully"


  Scenario: Posts List
    Given I am on the home page
    Then I should see all post

  Scenario: Create Post
    Given I am on create post page
    When I fill "Title" with "new"
    And I fill "Body" with "new body"
    Then I was created Post

  Scenario: Show detail Post
    Given I am on the home page
    When I click show post
    Then I should success show detail post

  Scenario: delete my Post
    Given I am on the post page
    When I click destroy post with "id" = "1"
    Then I should success delete my post

  Scenario: edit my Post
    Given I am on the post page
    When I click edit post with "id" = "1"
    And I edit "title" with "edit title"
    And I edit "body" with "edit body"
    Then I should success edit my post


