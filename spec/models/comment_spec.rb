require 'rails_helper'

RSpec.describe Comment, type: :model do
  user_id = User.ids.first
  article_id = Article.ids.first
  comment = Comment.new(commenter: "test", body: "testing", user_id: user_id, article_id: article_id, status: "public") 
  it "is valid with valid attributes" do
    expect(comment).to be_valid
  end

  it "is not valid without a commenter" do
    comment.commenter = nil
    expect(comment).to_not be_valid
  end

  it "is not valid without a body" do
    comment.body = nil
    expect(comment).to_not be_valid
  end

  it "is not valid because user not exist" do
    comment.user_id = 100
    expect(comment).to_not be_valid
  end
end
