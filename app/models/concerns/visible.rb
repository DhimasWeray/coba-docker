module Visible
  extend ActiveSupport::Concern

  VALID_STATUSES = ['public', 'private', 'archived']

  included do
    validates :status, inclusion: { in: VALID_STATUSES }
  end

  class_methods do
    VALID_STATUSES.each do |s|
      define_method("#{s}_count") do
        where(status: s).count
      end
    end
  end

  def archived?
    status == 'archived'
  end
end
